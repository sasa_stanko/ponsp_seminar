# Seminar za kolegij Parametarski ovisan nelinearan svojstveni problem (PONSP)

U ovom repozitoriju mogu se pronaći algoritmi za određivanje jednog ili više dominantnih polova LTI dinamičkih sustava kao i funkcije za njihovu analizu i usporedbu.

### Implementirani algoritmi
* `DPA` (Dominant Pole Algorithm)
* `RQI` (Rayleigh Quotient Iteration)
* `MDP` (MIMO `DPA`)
* `gMDP` (`MDP` za općenitu funkciju prijenosa)

### Implementirane tehnike
* Potprostorno ubrzanje
* Deflacija
* Obje tehnike istovremeno

Nisu sve tehnike implementirane na svim algoritmima.

Opišimo način na koji je moguće doći do imena algoritama na kojima su primijenjene ove tehnike. Neka je `alg` naziv osnovnog algoritma. Potprostorno ubrzani algoritam se zove `algsa`, algoritam s deflacijom se zove `algd`, a algoritam koji koristi obje tehnike istovremeno se zove `saalg`. Jedino odstupanje od posljednjeg pravila je algoritam `gsamdp`.

### Implementirani alati
* Generatori test primjera
* Usporedba većeg broja metoda
* Usporedba kvalitete aproksimacije

----

Dajemo primjere kako testirati implementirano. U svakom odlomku se blokovi naredbi trebaju pozvati slijedno. Prije nekih blokova naredbi se nalazi njihov opis. Za ispravan rad potrebno je nalaziti se u direktoriju 'source'.

### Potprostorno ubrzanje

``dpasa1 = @(E, A, b, c, s, eps, max_iter) dpasa(E, A, b, c, s, eps, max_iter, 1);
dpasa2 = @(E, A, b, c, s, eps, max_iter) dpasa(E, A, b, c, s, eps, max_iter, 2);``


``[E, A, B, C] = get_example(1, 40, 2, 100);
E = full(E);
A = full(A);
B = full(B);
C = full(C);
b = sum(B,2);
c = sum(C,2);``

DPA vs DPAsa, prihvaća se i konjugirani dominantni pol
``compare(E, A, b, c, 'algs', {@dpa, @dpasa}, 'alg_names', {'DPA', 'DPAsa'}, 'rx', 0.05, 'ry', 0.2, 'm', 16, 'eps', 1e-8, 'max_iter', 15, 'conj_pole', true);``

DPA vs DPAsa, prihvaća se samo pol u gornjoj poluravnini
``compare(E, A, b, c, 'algs', {@dpa, @dpasa}, 'alg_names', {'DPA', 'DPAsa'}, 'rx', 0.05, 'ry', 0.2, 'm', 16, 'eps', 1e-8, 'max_iter', 15);``

RQI vs RQIsa
``compare(E, A, b, c, 'algs', {@rqi, @rqisa}, 'alg_names', {'RQI', 'RQIsa'}, 'rx', 0.05, 'ry', 0.2, 'm', 16, 'eps', 1e-8, 'max_iter', 15);``

DPAsa vs RQIsa
``compare(E, A, b, c, 'algs', {@dpasa, @rqisa}, 'alg_names', {'DPAsa', 'RQIsa'}, 'rx', 0.05, 'ry', 0.2, 'm', 16, 'eps', 1e-8, 'max_iter', 15);``

DPAsa1 vs DPAsa2
``compare(E, A, b, c, 'algs', {dpasa1, dpasa2}, 'alg_names', {'DPAsa1', 'DPAsa2'}, 'rx', 0.05, 'ry', 0.2, 'm', 16, 'eps', 1e-8, 'max_iter', 15);``


### Deflacija

``[E, A, B, C] = get_example(1, 40, 2, 100);
E = full(E);
A = full(A);
B = full(B);
C = full(C);
b = sum(B,2);
c = sum(C,2);``

``K = -E(1:100, 1:100);
M = E(101:end, 101:end);
alpha_c = 0.002;
min_eig = eigs(K, M, 1, 'sm');
s0 = (-alpha_c + i * sqrt(1-alpha_c^2)) * min_eig;``

``[~,Rr,lambdas] = get_residuals(E, A, b, c);``

DPAd
``[poles,X,Y,iters,res] = dpad(E, A, b, c, repmat(s0, 1, 64), 1e-10, 60);``
``[ranks, tot_res] = find_ranks(Rr, lambdas, poles);
plot(ranks);``

Na ovom plotu se bolje vidi kvaliteta aproksimacije
``compare_bode_few(E, A, b, c, X(:,1:32), Y(:,1:32), linspace(0, 0.2, 50), false);``

Na ovom plotu su prikazane i relativne greške
``compare_bode_few(E, A, b, c, X(:,1:32), Y(:,1:32), linspace(0, 0.2, 50), true);``


### Obje tehnike

``[E, A, B, C] = get_example(1, 40, 2, 100);
E = full(E);
A = full(A);
B = full(B);
C = full(C);
b = sum(B,2);
c = sum(C,2);``

``K = -E(1:100, 1:100);
M = E(101:end, 101:end);
alpha_c = 0.002;
min_eig = eigs(K, M, 1, 'sm');
s0 = (-alpha_c + i * sqrt(1-alpha_c^2)) * min_eig;``

``[~,Rr,lambdas] = get_residuals(E, A, b, c);``

DPAd, DPAsa+d, SADPA
``[poles_d,X_d,Y_d,iters_d,res_d] = dpad(E, A, b, c, repmat(s0, 1, 32), 1e-10, 100, true);
[poles_sa1d,X_sa1d,Y_sa1d,iters_sa1d,res_sa1d] = dpasad(E, A, b, c, repmat(s0, 1, 32), 1e-10, 30, 1);
[poles_sa2d,X_sa2d,Y_sa2d,iters_sa2d,res_sa2d] = dpasad(E, A, b, c, repmat(s0, 1, 32), 1e-10, 30, 2);
[poles_sa1,X_sa1,Y_sa1,iters_sa1,res_sa1] = sadpa(E, A, b, c, s0, 32, 1e-10, 30, 1);
[poles_sa2,X_sa2,Y_sa2,iters_sa2,res_sa2] = sadpa(E, A, b, c, s0, 32, 1e-10, 30, 2);``

Prikaz koraka potrebnih za pojedini pol
``plot(iters_d); hold on; plot(iters_sa1d); plot(iters_sa2d); plot(iters_sa1); plot(iters_sa2); hold off; legend('d', 'sa1d', 'sa2d', 'sa1', 'sa2');``

Računanje i prikaz pronađenih rankova
``[ranks_d, sum_res_d] = find_ranks(Rr, lambdas, poles_d);
[ranks_sa1d, sum_res_sa1d] = find_ranks(Rr, lambdas, poles_sa1d);
[ranks_sa2d, sum_res_sa2d] = find_ranks(Rr, lambdas, poles_sa2d);
[ranks_sa1, sum_res_sa1] = find_ranks(Rr, lambdas, poles_sa1);
[ranks_sa2, sum_res_sa2] = find_ranks(Rr, lambdas, poles_sa2);``

``plot(sort(ranks_d)); hold on; plot(sort(ranks_sa1d)); plot(sort(ranks_sa2d)); plot(sort(ranks_sa1)); plot(sort(ranks_sa2)); hold off; legend('d', 'sa1d', 'sa2d', 'sa1', 'sa2');``

### MIMO sustavi

``[E, A, B, C] = get_example(1, 40, 2, 100);
E = full(E);
A = full(A);
B = full(B);
C = full(C);
Cs = C(:,1:size(B,2));``

``K = -E(1:100, 1:100);
M = E(101:end, 101:end);
alpha_c = 0.002;
min_eig = eigs(K, M, 1, 'sm');
s0 = (-alpha_c + i * sqrt(1-alpha_c^2)) * min_eig;``

Usporedba naše i Rommesove implementacije MDP algoritma; Ovaj poziv se ruši zbog Rommesove implementacije u kojoj vrijednosti postanu Inf/NaN.
``compare(E, A, B, Cs, 'algs', {@mdp_rommes, @mdp}, 'alg_names', {'MPD Rommes', 'MDP'}, 'r', 0.01, 'm', 8);``

Potvrda da naša implementacija funkcionira
``compare(E, A, B, Cs, 'algs', {@mdp}, 'alg_names', {'MPD'}, 'r', 0.01, 'm', 8);``

Usporedba različitih implementacija gMDP algoritma; Prvo obična implementacija uspoređena s modificiranim korakom
``compare(E, A, B, C, 'algs', {@gmdp, @gmdp_modified}, 'alg_names', {'gMPD', 'gMDP mod'}, 'rx', 0.0075, 'ry', 0.015, 'm', 12, 'max_iter', 40);``

Potom modificirani uspoređen s modificiranim kod kojeg se singularni vektori računaju samo jednom
``compare(E, A, B, C, 'algs', {@gmdp_modified, @gmdp_modified_out}, 'alg_names', {'gMPD mod', 'gMDP out'}, 'rx', 0.0075, 'ry', 0.015, 'm', 12, 'max_iter', 40);``

Usporedba MDP i gMDP za kvadratni problem
``compare(E, A, B, Cs, 'algs', {@mdp, @gmdp}, 'alg_names', {'MPD', 'gMDP'}, 'r', 0.01, 'm', 8, 'max_iter', 40);``
