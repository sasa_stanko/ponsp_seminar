function compare_bode_few(E, A, b, c, X, Y, ss, plot_error)
%COMPARE_BODE_FEW Compare transfer function with few of its approximations.
%
%  Number of columns of matrices X and Y must be divisible by 4.
%
%  (E, A, b, c) = Descriptor SISO system.
%  X = Approximation of right dominant eigenvectors.
%  Y = Approximation of left dominant eigenvectors.
%  ss = Grid on which function is plotted.
%  plot_error = Whether to plot relative error.

  if nargin < 7, ss = linspace(0, 10, 200); end
  if nargin < 8, plot_error = true; end

  % original system
  H = @(s) c' * ((s * E - A) \ b);
  y = arrayfun(H, 1i * ss);

  semilogy(ss, abs(y));
  hold on;

  % projected system
  for i = [4,2,1]
    sz = size(X, 2) / i;

    E_til = Y(:,1:sz)' * E * X(:,1:sz);
    A_til = Y(:,1:sz)' * A * X(:,1:sz);
    b_til = Y(:,1:sz)' * b;
    c_til = X(:,1:sz)' * c;

    % evaluate transfer function
    H_til = @(s) c_til' * ((s * E_til - A_til) \ b_til);
    y_til = arrayfun(H_til, 1i * ss);
    r_err = abs((y - y_til) ./ y);

    semilogy(ss, abs(y_til));
    if plot_error
      semilogy(ss, abs(r_err));
    end
  end

  if plot_error
    legend('gain', 'approx 1/4', 'rel. error 1/4', ...
           'approx 1/2', 'rel. error 1/2', ...
           'approx', 'rel. error');
  else
    legend('gain', 'approx 1/4', 'approx 1/2', 'approx');
  end

  hold off;
end