function plot_transfer(E, A, b, c, ss)
%PLOT_TRANSFER Plot transfer function.
%
%  (E, A, b, c) = Descriptor SISO system.
%  ss = Grid on which function is plotted.

  if nargin < 5
    ss = linspace(0, 5, 100);
  end

  H = @(s) c' * ((s * E - A) \ b);
  y = arrayfun(H, 1i * ss);

  figure;
  subplot(1,2,1);
  semilogy(ss, abs(y));
  legend('gain');
  subplot(1,2,2);
  plot(ss, angle(y));
  legend('phase');

  pos = get(gcf, 'Position');
  pos(3) = 2.5 * pos(4);
  set(gcf, 'Position', pos);
end