function [E, A, B, C] = linearize(M, C_u, K, B2, E2, H1, G)
%LINEARIZE Compute linearization of quadratic vibrational system.
%
%  M = Mass matrix.
%  C_u = Internal damping matrix (alpha_c * C_crit).
%  K = Stiffness matrix.
%  B2 = Control matrix.
%  E2 = Primary excitation matrix.
%  H1 = Output matrix.
%  G = Gain matrix.
%
%  Resulting system is
%      Ex'(t) = Ax(t) + Bw(t)
%        z(t) = C*x(t)

  n = size(M, 1);
  A = zeros(2*n, 2*n);
  A(1:n, n+1:2*n) = -K;
  % A(1:n, n+1:2*n) = speye(n);
  A(n+1:2*n, 1:n) = -K;
  A(n+1:2*n, n+1:2*n) = -(C_u + B2 * G * B2');
  B = [sparse([], [], [], n, size(E2, 2), 0); E2];
  C = [H1, sparse([], [], [], size(H1, 1), n, 0)]';
  E = [-K, sparse(n, n); sparse(n, n), M];
  % E = [speye(n), sparse(n, n); sparse(n, n), M];
end