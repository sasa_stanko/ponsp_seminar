function [u, mu, z] = largest_eig(A)
%LARGEST_EIG Eigentriplet largest in magnitude
  [U, mu, V] = eig(A, 'vector');
  [~, idx] = max(abs(mu));
  u = U(:,idx);
  z = V(:,idx);
  mu = mu(idx);
end