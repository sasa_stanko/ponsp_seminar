function [lambdas, X, Y, iters, residuals] = ...
  gmdpd(E, A, B, C, ss, eps, max_iter, add_conj)
%GMDPD MDP algorithm with deflation for general transfer function
%
%  (E, A, B, C) = Descriptor system with D = 0.
%  ss = Vector of initial pole estimates.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations for each initial estimate.
%  add_conj = When a strictly complex pole is found, should its complex
%             conjugate pair be added too.
%             Default is false.
%
%  Returns as many approximate dominant poles as there are initial estimates.
%  Number of iterations per each pole is returned in iters vector.

  % TODO: Make it possible to set p, number of desired poles.
  %       In case there are no more initial estimates, use the
  %       last obtained approximation to dominant pole.

  if nargin < 8, add_conj = false; end

  n = length(B);
  p = length(ss);
  lambdas = zeros(2*p, 1);
  X = zeros(n, 2*p);
  Y = zeros(n, 2*p);
  iters = zeros(p, 1);
  residuals = zeros(1, p * max_iter);

  k = 0;
  step = 0;
  for i = 1:p
    s = ss(i);
    iter = 0;
    converged = false;
    [R,~] = linsolve(s*E-A, B);
    H = C' * R;
    [u, ~, z] = largest_sing(H);
    while ~converged && iter < max_iter
      iter = iter + 1;
      step = step + 1;
      [v, w] = step_solve(E, A, s, B*z, C*u);
      s = (w' * A * v) / (w' * E * v);
      x = v / norm(v);
      y = w / norm(w);
      residuals(step) = max(norm(A*x - s*E*x), norm(y'*A - s*y'*E));
      converged = (residuals(step) < eps);
    end
    iters(i) = iter;
    [v, w] = escale(v, w, E);
    k = k + 1;
    lambdas(k) = s;
    X(:,k) = v;
    Y(:,k) = w;
    if add_conj && abs(imag(s)) > eps
      k = k + 1;
      lambdas(k) = conj(s);
      X(:,k) = conj(v);
      Y(:,k) = conj(w);
      B = B - E * [real(v), imag(v)] * ([2*real(w), 2*imag(w)]' * B);
      C = C - E' * [real(w), imag(w)] * ([2*real(v), 2*imag(v)]' * C);
    else
      B = B - E * v * (w' * B);
      C = C - E' * w * (v' * C);
    end
  end
  lambdas = lambdas(1:k);
  X = X(:,1:k);
  Y = Y(:,1:k);
  residuals = residuals(1:step);
end