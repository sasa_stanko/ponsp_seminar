function plot_residuals(R, Rr, lambdas)
%PLOT_RESIDUALS Plot residuals of transfer function for the given system.
%
%  R = Norms of residuals.
%  Rr = Norms of residuals divided by the absolute real part of the eigenvalue.
%  lambdas = Eigenvalues for which the residuals are computed (optional).

  n = length(R);

  % plot residuals
  x = 0.5 * [1; rep2(3:2:2*n-1); 2*n+1];
  plot(x, rep2(R), 'b', x, rep2(Rr), 'r');

  % make plot look pretty
  legend({'$\|R_i\|$', '$\|R_i\|/|Re(\lambda_i)|$'},'Interpreter','latex')
  xlim([0.8, n+0.2]);
  height = max([R; Rr]);
  ylim([-0.05*height, 1.05*height]);

  % write eigenvalues to the plot
  if nargin == 3
    yoffset = 0.025 * height;
    for i = 1:n
      text(i, max(R(i), Rr(i)) + yoffset, num2str(lambdas(i), 2), ...
           'HorizontalAlignment', 'center');
    end
  end
end

% repeat every value in v twice
% result is column vector
function w = rep2(v)
  w = zeros(2*length(v), 1);
  w(1:2:end) = v;
  w(2:2:end) = v;
end