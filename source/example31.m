function [M, alpha_c, K, B2, H1, E2] = example31(j, k, n)
%Example 3.1. from the article "Semi-active damping optimization of vibrational
%systems using the parametric dominant pole algorithm".
%
%   j = Parameter j from the example, in range [1, n-21].
%   k = Parameter k from the example, in range [1, n-29].
%   n = Parameter n from the example, default is 1800.
%
%   All generated matrices are sparse.

  if nargin == 2
    n = 1800;
  end
  s1 = round(n / 3);
  s2 = round(n / 1.8);

  m = [n - 0.5*s1 - 2*(1:s1), s1 - 0.5*(s1+1:s2-1), (s2:n) - (n-s2)];
  M = sparse(1:n, 1:n, m, n, n, n);
  alpha_c = 0.002;
  K = 2 * gallery('tridiag', n);
  B2 = sparse([j, j+1, j+10, j+11, j+20, j+21], 1:6, ones(1, 6), n, 6, 6);
  H1 = sparse(1:40, s1-20:s1+19, ones(1, 40), 40, n, 40);
  E2 = sparse(k:k+29, 1:30, ones(1, 30), n, 30, 30);
end