function [lambda, v, w, iter, residuals] = ...
  rqisa(E, A, b, c, s, eps, max_iter, dominance, k_max, k_min)
%RQISA Two-sided Rayleigh quotient iteration with subspace acceleration
%
%  (E, A, b, c) = Descriptor SISO system.
%  s = initial pole estimate.
%  eps = tolerance << 1.
%  max_iter = Maximum number of iterations.
%  dominance = Type of dominance considered. Default value is 1.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i)/abs(real(lambda_i)), norm(x_i) = norm(y_i) = 1
%  k_max = When searching subspaces reach this dimension, restart is applied.
%          Default value is Inf.
%  k_min = When restart is applied, only this many vectors are kept.
%          Default value is 2.

  if nargin < 8, dominance = 1; end
  if nargin < 9, k_max = Inf; end
  if nargin < 10, k_min = 2; end

  [v, w] = step_solve(E, A, s, b, c);
  v = v / norm(v);
  w = w / norm(w);
  V = v;
  W = w;
  s = (w' * A * v) / (w' * E * v);

  iter = 1;
  residuals = zeros(1, max_iter);
  residuals(1) = norm(A*v - s*E*v);
  converged = (residuals(1) < eps);
  while ~converged && iter < max_iter
    iter = iter + 1;
    if size(V, 2) >= k_max
      [V,~] = qr(V * X_til(:,k_min), 0);
      [W,~] = qr(W * Y_til(:,k_min), 0);
    end
    [v, w] = step_solve(E, A, s, E * v, E' * w);
    V = mgs(V, v);
    W = mgs(W, w);
    [s_til, X_til, Y_til] = sorted_poles(W'*E*V, W'*A*V, W'*b, V'*c, dominance);
    s = s_til(1);
    v = V * X_til(:,1);
    v = v / norm(v);
    w = W * Y_til(:,1);
    w = w / norm(w);
    residuals(iter) = max(norm(A*v - s*E*v), norm(w'*A - s*w'*E));
    converged = (residuals(iter) < eps);
  end

  lambda = s;
  residuals = residuals(1:iter);
end