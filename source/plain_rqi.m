function [s, v, w, iter, residuals] = plain_rqi(A, B, v, w, s, eps, max_iter)
%PLAIN_RQI Two-sided Rayleigh quotient iteration for eigenvalue pencil (A, B)
%
%  (A, B) = Matrices in pencil A*x = lambda*B*x.
%  v, w = initial right and left eigenvector estimate.
%  s = initial eigenvalue estimate.
%  eps = tolerance << 1.
%  max_iter = Maximum number of iterations.
%
%  Returns (lambda, x, y, iter, residuals).

  v = v / norm(v);
  w = w / norm(w);

  residuals = zeros(1, max_iter);
  for iter = 1:max_iter
    [v, w] = step_solve(B, A, s, B * v, B' * w);
    s = (w' * A * v) / (w' * B * v);
    v = v / norm(v);
    w = w / norm(w);
    residuals(iter) = max(norm(A*v - s*B*v), norm(w'*A - s*w'*B));
    if residuals(iter) < eps
      break
    end
  end
  residuals = residuals(1:iter);
end