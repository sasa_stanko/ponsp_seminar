function [v, w] = step_solve(E, A, s, b, c)
%STEP_SOLVE Step used in DPA and RQI algorithms
%
%  b = Right-hand side for linear system computing v.
%  c = Right-hand side for linear system computing w.

  % prepare options for linsolve
  optu.UT = true;
  optl.LT = true;
  optuh = optu; optuh.TRANSA = true;
  optlh = optl; optlh.TRANSA = true;

  % solve using LU factorization
  [L, U, p] = lu(s * E - A, 'vector');
  [v,~] = linsolve(L, b(p), optl);
  [v,~] = linsolve(U, v, optu);
  [w,~] = linsolve(U, c, optuh);
  [w,~] = linsolve(L, w, optlh);
  w(p) = w;
end