function [s, x, y] = sorted_poles(E, A, B, C, dominance)
%SORTED_POLES Sort the poles of the transfer function by their dominance.
%
%  (E, A, B, C) = Descriptor system with D = 0.
%  dominance = Type of dominance considered. Default value is 1.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i)/abs(real(lambda_i)), norm(x_i) = norm(y_i) = 1
%
%  s = Poles sorted by their dominance.
%  x = Right eigenvectors for the given poles.
%  y = Left eigenvectors for the given poles.
%
%  Note that x and y are not scaled in any particular way.

  if nargin < 5
    dominance = 1;
  end

  [X, D, Y] = eig(A, E, 'vector');
  if dominance == 1
    R = abs(cnorm(C' * X) .* cnorm(B' * Y) ./ dot(Y, E * X)).';
  elseif dominance == 2
    X = bsxfun(@rdivide, X, cnorm(X));
    Y = bsxfun(@rdivide, Y, cnorm(Y));
    R = (cnorm(C' * X) .* cnorm(B' * Y)).';
  end
  R = R ./ abs(real(D));
  [~, indices] = sort(R, 'descend');
  s = D(indices);
  x = X(:,indices);
  y = Y(:,indices);
end