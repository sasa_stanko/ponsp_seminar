function [lambdas, X, Y, iters, residuals] = ...
  dpasad(E, A, b, c, ss, eps, max_iter, dominance, k_max, k_min)
%DPASAD Dominant Pole Algorithm with deflation where each iteration uses
%       subspace acceleration.
%
%  (E, A, b, c) = Descriptor SISO system.
%  ss = Vector of initial pole estimates.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations for each initial estimate.
%  dominance = Type of dominance considered. Default value is 1.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i)/abs(real(lambda_i)), norm(x_i) = norm(y_i) = 1
%  k_max = When searching subspaces reach this dimension, restart is applied.
%          Default value is Inf.
%  k_min = When restart is applied, only this many vectors are kept.
%          Default value is 2.
%
%  Returns as many approximate dominant poles as there are initial estimates.
%  Number of iterations per each pole is returned in iters vector.

  % TODO: Make it possible to set p, number of desired poles.
  %       In case there are no more initial estimates, use the
  %       last obtained approximation to dominant pole.

  if nargin < 8, dominance = 1; end
  if nargin < 9, k_max = Inf; end
  if nargin < 10, k_min = 2; end

  n = length(b);
  p = length(ss);
  lambdas = zeros(2*p, 1);
  X = zeros(n, 2*p);
  Y = zeros(n, 2*p);
  iters = zeros(p, 1);
  residuals = zeros(1, p * max_iter);

  k = 0;
  step = 0;
  for i = 1:p
    s = ss(i);
    iter = 0;
    converged = false;
    V = [];
    W = [];
    while ~converged && iter < max_iter
      iter = iter + 1;
      step = step + 1;
      if size(V, 2) >= k_max
        [V,~] = qr(V * X_til(:,k_min), 0);
        [W,~] = qr(W * Y_til(:,k_min), 0);
      end
      [v, w] = step_solve(E, A, s, b, c);
      V = mgs(V, v);
      W = mgs(W, w);
      [s_til, X_til, Y_til] = sorted_poles(W'*E*V, W'*A*V, W'*b, V'*c, dominance);
      s = s_til(1);
      v = V * X_til(:,1);
      v = v / norm(v);
      w = W * Y_til(:,1);
      w = w / norm(w);
      residuals(step) = max(norm(A*v - s*E*v), norm(w'*A - s*w'*E));
      converged = (residuals(step) < eps);
    end
    iters(i) = iter;
    [v, w] = escale(v, w, E);
    k = k + 1;
    lambdas(k) = s;
    X(:,k) = v;
    Y(:,k) = w;
    if abs(imag(s)) > eps
      k = k + 1;
      lambdas(k) = conj(s);
      X(:,k) = conj(v);
      Y(:,k) = conj(w);
      b = b - E * ([real(v), imag(v)] * (2 * ([real(w), imag(w)]' * b)));
      c = c - E' * ([real(w), imag(w)] * (2 * ([real(v), imag(v)]' * c)));
    else
      b = b - E * v * (w' * b);
      c = c - E' * w * (v' * c);
    end
  end
  residuals = residuals(1:step);
  lambdas = lambdas(1:k);
  X = X(:,1:k);
  Y = Y(:,1:k);
end