function [lambda, x, y, iter, residuals] = ...
  mdpsa(E, A, B, C, s, eps, max_iter, dominance, k_max, k_min)
%MDPSA MDP with subpsace acceleration
%
%  (E, A, B, C) = Descriptor MIMO system.
%  s = Initial pole estimate.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations.
%  dominance = Type of dominance considered. Default value is 1.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i)/abs(real(lambda_i)), norm(x_i) = norm(y_i) = 1
%  k_max = When searching subspaces reach this dimension, restart is applied.
%          Default value is Inf.
%  k_min = When restart is applied, only this many vectors are kept.
%          Default value is 2.

  if nargin < 8, dominance = 1; end
  if nargin < 9, k_max = Inf; end
  if nargin < 10, k_min = 2; end

  V = [];
  W = [];

  iter = 0;
  converged = false;
  residuals = zeros(1, max_iter);
  [R,~] = linsolve(s*E-A, B);
  H = C' * R;
  [u, ~, z] = largest_eig(H);
  while ~converged && iter < max_iter
    iter = iter + 1;
    if size(V, 2) >= k_max
      [V,~] = qr(V * X_til(:,k_min), 0);
      [W,~] = qr(W * Y_til(:,k_min), 0);
    end
    [v, w] = step_solve(E, A, s, B*u, C*z);
    V = mgs(V, v);
    W = mgs(W, w);
    [s_til, X_til, Y_til] = sorted_poles(W'*E*V, W'*A*V, W'*B, V'*C, dominance);
    s = s_til(1);
    x = V * X_til(:,1);
    x = x / norm(x);
    y = W * Y_til(:,1);
    y = y / norm(y);
    residuals(iter) = max(norm(A*x - s*E*x), norm(y'*A - s*y'*E));
    converged = (residuals(iter) < eps);
  end
  lambda = s;
  residuals = residuals(1:iter);
end