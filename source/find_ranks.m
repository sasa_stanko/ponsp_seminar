function [pole_ranks, res_sum] = find_ranks(R, lambdas, poles)
% find rank of each computed pole knowing residuals and eigenvalues

  ranks = get_ranks(R);
  pole_ranks = zeros(size(poles));
  res_sum = 0;
  for i = 1:length(poles)
    pole = poles(i);
    [~, idx] = min(abs(lambdas - pole));
    pole_ranks(i) = ranks(idx);
    res_sum = res_sum + R(idx);
  end
end


function ranks = get_ranks(R)
% find rank of each residual

  n = length(R);
  ranks = 1:n;
  [R, idxs] = sort(R, 'descend');
  eps = 8e-16;
  for i = 2:n
    ranks(i) = ranks(i-1);
    if (R(i-1) - R(i)) > eps * R(i-1)
      ranks(i) = ranks(i) + 1;
    end
  end
  ranks(idxs) = ranks;
end