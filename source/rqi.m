function [lambda, x, y, iter, residuals] = rqi(E, A, b, c, s, eps, max_iter)
%RQI Two-sided Rayleigh quotient iteration 
%    (Rommes - Phd Thesis, Algorithm 2.2)
%
%  (E, A, b, c) = Descriptor SISO system.
%  s = initial pole estimate.
%  eps = tolerance << 1.
%  max_iter = Maximum number of iterations.

  [v, w] = step_solve(E, A, s, b, c);
  v = v / norm(v);
  w = w / norm(w);

  iter = 0;
  converged = false;
  residuals = zeros(1, max_iter);
  while ~converged && iter < max_iter
    iter = iter + 1;
    [v, w] = step_solve(E, A, s, E * v, E' * w);
    s = (w' * A * v) / (w' * E * v);
    v = v / norm(v);
    w = w / norm(w);
    residuals(iter) = max(norm(A*v - s*E*v), norm(w'*A - s*w'*E));
    converged = (residuals(iter) < eps);
  end
  
  lambda = s;
  x = v;
  y = w;
  residuals = residuals(1:iter);
end