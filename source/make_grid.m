function G = make_grid(z, rx, ry, m)
%MAKE_GRID Create (2m+1)x(2m+1) grid of complex numbers centered in z.
%
%  z = Center of the grid.
%  rx = Half-width of the grid.
%  ry = Half-height of the grid.
%  m = Number of points in one direction.

  re = real(z);
  im = imag(z);
  G = zeros(2*m+1);
  G = G + repmat(linspace(re-rx, re+rx, 2*m+1), 2*m+1, 1);
  G = G + repmat(1i * linspace(im-ry, im+ry, 2*m+1).', 1, 2*m+1);
end