function compare_multi(E, A, B, C, ss, varargin)
%COMPARE_MULTI Compares different methods for finding several dominant poles.
%
%  Depending on the number of columns of B and C, SISO or MIMO version
%  of algorithms is called.
%
%  (E, A, B, C) = Descriptor system with D = 0.
%  ss = Inital pole estimate(s).
%
%  Parameters
%  dominance = Definition of dominance considered.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i), y_i' * E * x_j = delta_{ij}
%              Default value is 1.
%  eps = Tolerance. Passed to the algorihm.
%      Default value is 1e-10.
%  max_iter = Maximum number of iterations. Passed to the algorithm.
%      Default values is 20.
%
%  Whole ss is used to initialize the algorithm with deflation.
%  Only ss(1) is used to initialize the algorithm with subsapce acceleration
%  and deflation.

  % parse parameters
  p = inputParser;
  addParameter(p, 'dominance', 1);
  addParameter(p, 'eps', 1e-10);
  addParameter(p, 'max_iter', 20);
  parse(p, varargin{:});

  dominance = p.Results.dominance;
  eps = p.Results.eps;
  max_iter = p.Results.max_iter;

  % determine which algorithms to call
  if size(B, 2) == 1 && size(C, 2) == 1
    algd = @dpad;
    algsa = @sadpa;
    alg_names = {'DPAd', 'SADPA'};
  elseif size(B, 2) == size(C, 2)
    algd = @mdpd;
    algsa = @samdp;
    alg_names = {'MDPd', 'SAMDP'};
  else
    algd = @gmdpd;
    algsa = @gsamdp;
    alg_names = {'gMDPd', 'gSAMDP'};
  end

  % prepare parameters for subspace accelerated method
  s = ss(1);
  p = length(ss);

  % run tests
  results = cell(2, 1);
  t0 = tic;
  [poles, X, Y, iters] = algd(E, A, B, C, ss, eps, max_iter);
  dt = toc(t0);
  results{1} = struct('poles', poles, 'iters', iters, 'X', X, 'Y', Y, 't', dt);
  t0 = tic;
  [poles, X, Y, iters] = algsa(E, A, B, C, s, p, eps, max_iter);
  dt = toc(t0);
  iters = add_conj_iters(poles, iters, eps);
  results{2} = struct('poles', poles, 'iters', iters, 'X', X, 'Y', Y, 't', dt);

  % get residuals and eigenvalues for rank computation
  [R, Rr, lambdas] = get_residuals(E, A, B, C);
  if dominance ~= 2
    R = Rr;
  end

  % show results
  for k = 1:2
    iters = results{k}.iters;
    poles = results{k}.poles;
    ranks = find_ranks(R, lambdas, poles);
    [Rk, Rrk] = compute_residuals(E, B, C, results{k}.X, results{k}.Y, poles);
    fprintf('\n');
    print_table(alg_names{k}, iters, poles, Rk, Rrk, results{k}.t, ranks);
  end
end


function print_table(header, iters, poles, R_approx, Rr_approx, t, ranks)
  padding = 58 - length(header);
  left_pad = repmat(' ', 1, fix(padding/2));
  right_pad = repmat(' ', 1, padding - fix(padding/2));
  fprintf('|%s%s%s|\n', left_pad, header, right_pad);
  fprintf('----------+-------------------+-----------+-----------+-----\n');
  fprintf('Iteration |        Pole       |     R     |     Rr    | Rank\n');
  p = length(iters);
  iters = cumsum(iters);
  % print stats for each iteration
  for i = 1:p
    pole_str = num2str(poles(i), 3);
    Ri = num2str(R_approx(i));
    Rri = num2str(Rr_approx(i));
    fprintf(' %8d | %17s | %9s | %9s | %4d\n', iters(i), pole_str, ...
            Ri, Rri, ranks(i));
  end
  fprintf('----------+-------------------+-----------+-----------+-----\n');
  fprintf('Sum of residuals R = %s\n', num2str(sum(R_approx)));
  fprintf('Sum of residuals Rr = %s\n', num2str(sum(Rr_approx)));
  fprintf('Number of iterations = %d\n', iters(end));
  fprintf('Duration = %f\n', t);
end

function iters = add_conj_iters(poles, iters, eps)
% add 0 to iters for each complex conjugate pair
% since iters contains only one entry for such two poles.
% pairs are considered complex conjugate if abs(s - conj(s)) < eps

  new_iters = zeros(size(poles));
  new_iters(1) = iters(1);
  curr = 2;
  for i = 2:length(poles)
    if abs(poles(i) - conj(poles(i-1))) >= eps
      new_iters(i) = iters(curr);
      curr = curr + 1;
    else
      new_iters(i) = 0;
    end
  end
  iters = new_iters;
end
