function [s, j_start] = pick_next_start(s, s_til, eps)
%PICK_NEXT_START Picks best approximation of the next pole
%
%  s = Current pole.
%  s_til = List of candidates sorted in descending order of dominance.
%  eps = Tolerance << 1.
%
%  Returns next pole approximation (s) and index of the next pole in the
%  s_til (j_start). Index j_start is used to discard vectors in search spaces
%  that belong to discarded candidates.

  for i = 1:length(s_til)
    if abs(s - s_til(i)) > eps && abs(conj(s) - s_til(i)) > eps
      s = s_til(i);
      j_start = i + 1;
      return
    end
  end

  % if everything else failed, take the last element
  s = s_til(end);
  j_start = length(s_til) + 1;
end