function [lambdas, X, Y, iters, residuals] = ...
  dpad(E, A, b, c, ss, eps, max_iter, add_conj)
%DPAD Dominant Pole Algorithm with deflation (Rommes - Phd Thesis, Alg 3.1)
%
%  (E, A, b, c) = Descriptor SISO system.
%  ss = Vector of initial pole estimates.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations for each initial estimate.
%  add_conj = When a strictly complex pole is found, should its complex
%             conjugate pair be added too.
%             Default is false.
%
%  Returns as many approximate dominant poles as there are initial estimates.
%  Number of iterations per each pole is returned in iters vector.

  % TODO: Make it possible to set p, number of desired poles.
  %       In case there are no more initial estimates, use the
  %       last obtained approximation to dominant pole.

  if nargin < 8, add_conj = false; end

  n = length(b);
  p = length(ss);
  lambdas = zeros(2*p, 1);
  X = zeros(n, 2*p);
  Y = zeros(n, 2*p);
  iters = zeros(p, 1);
  residuals = zeros(1, p * max_iter);

  k = 0;
  step = 0;
  for i = 1:p
    s = ss(i);
    iter = 0;
    converged = false;
    while ~converged && iter < max_iter
      iter = iter + 1;
      step = step + 1;
      [v, w] = step_solve(E, A, s, b, c);
      s = (w' * A * v) / (w' * E * v);
      x = v / norm(v);
      y = w / norm(w);
      residuals(step) = max(norm(A*x - s*E*x), norm(y'*A - s*y'*E));
      converged = (residuals(step) < eps);
    end
    iters(i) = iter;
    [v, w] = escale(v, w, E);
    k = k + 1;
    lambdas(k) = s;
    X(:,k) = v;
    Y(:,k) = w;
    if add_conj && abs(imag(s)) > eps
      k = k + 1;
      lambdas(k) = conj(s);
      X(:,k) = conj(v);
      Y(:,k) = conj(w);
      b = b - E * ([real(v), imag(v)] * (2 * ([real(w), imag(w)]' * b)));
      c = c - E' * ([real(w), imag(w)] * (2 * ([real(v), imag(v)]' * c)));
    else
      b = b - E * v * (w' * b);
      c = c - E' * w * (v' * c);
    end
  end
  lambdas = lambdas(1:k);
  X = X(:,1:k);
  Y = Y(:,1:k);
  residuals = residuals(1:step);
end