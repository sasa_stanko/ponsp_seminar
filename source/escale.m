function [v, w] = escale(v, w, E)
%ESCALE Scale both v and w to ensure w' * E * v == 1.
%
%  Scaling only one vector as in Rommes's PhD makes huge difference in their
%  scale. For example, one would have norm around 1e-16 and the other would
%  have norm around 1e+16. This scaling equally affects both vectors.

  v = v / norm(v);
  w = w / norm(w);
  p = w' * E * v;
  v = v / (p / sqrt(abs(p)));
  w = w / sqrt(abs(p));
end