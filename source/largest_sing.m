function [u, sigma, z] = largest_sing(H)
%LARGEST_SING Largest singular value and its singular vectors
  [U, S, V] = svd(H);
  u = U(:,1);
  z = V(:,1);
  sigma = S(1, 1);
end