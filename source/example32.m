function [M, alpha_c, K, B2, H1, E2] = example32(j, k, d)
%Example 3.2. Case I from the article "Semi-active damping optimization of 
%vibrational systems using the parametric dominant pole algorithm".
%
%   j = Parameter j from the example, in range [1, 2*d-10].
%   k = Parameter k from the example, in range [1, 2*d-10].
%   d = Parameter d from the example, default is 800.
%
%   All generated matrices are sparse.

  if nargin == 2
    d = 800;
  end
  n = 2 * d + 1;
  s1 = round(0.75 * d);
  s2 = round(0.5 * s1);
  s3 = 2 * s1;

  m = [3.98 + 0.02*(1:s1), 34 - 0.03*(s1+1:d), 23 - 0.01*(d+1:n-1), 10];
  M = sparse(1:n, 1:n, m, n, n, n);

  alpha_c = 0.005;

  k1 = 3;
  k2 = 1;
  k3 = 2;
  T = gallery('tridiag', d);
  Z = sparse([], [], [], d, d, 0);
  z = sparse(d, 1, 1, d, 1, 1);
  K = [k1*T, Z, -k1 * z; Z, k2*T, -k2 * z; -k1 * z', -k2 * z', k1 + k2 + k3];

  H1 = sparse([1:10, 1:10], [s2+1:s2+10, s3+1:s3+10], ones(1, 20), 10, n, 20);
  E2 = sparse([1:5, d+1:d+5, n], 1:11, [5,4,3,2,1,5,4,3,2,1,10]);
  B2 = sparse([j, j+10, k, k+10, j+1, j+11, k+1, k+11], [1:4, 1:4], ...
              [ones(1, 4), -ones(1, 4)], n, 4, 8);
end