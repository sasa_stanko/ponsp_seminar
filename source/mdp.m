function [lambda, x, y, iter, residuals] = mdp(E, A, B, C, s, eps, max_iter)
%MDP MIMO Dominant Pole Algorithm for square transfer function with
%    modified step
%   See Rommes - Phd Thesis, Alg 4.1
%
%  (E, A, B, C) = Descriptor MIMO system.
%  s = Initial pole estimate.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations per pole.
%
%  Returns p approximations of dominant poles.
%  Complex conjugate pair is counted as one pole.

  iter = 0;
  converged = false;
  residuals = zeros(1, max_iter);
  while ~converged && iter < max_iter
    iter = iter + 1;
    [R,~] = linsolve(s*E-A, B);
    H = C' * R;
    [u, ~, z] = largest_eig(H);
    [v, w] = step_solve(E, A, s, B*u, C*z);
    s = (w' * A * v) / (w' * E * v);
    x = v / norm(v);
    y = w / norm(w);
    residuals(iter) = max(norm(A*x - s*E*x), norm(y'*A - s*y'*E));
    converged = (residuals(iter) < eps);
  end
  lambda = s;
  residuals = residuals(1:iter);
end