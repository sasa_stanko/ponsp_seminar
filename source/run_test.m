function [lambdas, iters] = run_test(E, A, b, c, G, algorithm, eps, max_iter)
%RUN_TEST Runs algorithm which works on the descriptor system 
%         for each point of the grid.
%
%  (E, A, b, c) = Descriptor system.
%  G = Grid of starting points.
%  algorithm = Algorithm to be run. Its signature is @(E, A, b, c, s, eps).
%  eps = Tolerance. Passed to the algorihm.
%  max_iter = Maximum number of iterations. Passed to the algorithm.

  m = size(G, 1);
  n = size(G, 2);
  lambdas = zeros(m, n);
  iters = zeros(m, n);
  for i = 1:m
    for j = 1:n
      s = G(i,j);
      [lambdas(i,j), ~, ~, iters(i,j)] = ...
          algorithm(E, A, b, c, s, eps, max_iter);
    end
  end
end