function varargout = deflate(E, A, b, c, X, Y)
%DEFLATE Deflate system (E, A, b, c) using computed eigenvectors X, Y.
%        Assumes eigenvectors correspond to distinct eigenvalues of s*E-A.
%
%  (E, A, b, c) = Descriptor SISO system.
%  X = Computed right eigenvectors.
%  Y = Computed left eigenvectors
%
%  Returns deflated system (Ed, Ad, bd, cd) when 4 outputs are expected.
%  Returns deflated vectors (bd, cd) when 2 outputs are expeced.

  % normalize X and Y such that Y'*E*X = I
  d = dot(Y, E*X);
  Y = Y * diag(1 ./ d);

  % perform deflation
  bd = b - E * X * (Y' * b);
  cd = c - E' * Y * (X' * c);
  vidx = 1;
  if nargout == 4
    I = eye(size(E,1));
    Pl = I - E * X * Y';
    Pr = I - X * Y' * E;
    Ed = Pl * E * Pr;
    Ad = Pl * A * Pr;
    varargout{1} = Ed;
    varargout{2} = Ad;
    vidx = vidx + 2;
  end
  varargout{vidx} = bd;
  varargout{vidx+1} = cd;
end