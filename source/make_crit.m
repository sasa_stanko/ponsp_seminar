function C_crit = make_crit(M, K)
%MAKE_CRIT Create critical damping matrix for given M and K.
%
%  M = Mass matrix, must be diagonal.
%  K = Stiffness matrix.

  % perform elementwise square root
  % it is faster, but M needs to be diagonal
  M_half = sqrt(M);
  
  % compute C_crit
  C_crit = 2 * M_half * sqrtm(full((M_half \ K) / M_half)) * M_half;
end
