function [lambdas, X, Y, iters, residuals] = ...
  sadpa(E, A, b, c, s, p, eps, max_iter, dominance, k_max, k_min)
%SADPA Subspace Accelerated Dominant Pole Algorithm
%      See Rommes - Phd Thesis, Alg 3.2
%
%  (E, A, b, c) = Descriptor SISO system.
%  s = Initial pole estimate.
%  p = Number of poles desired.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations per pole.
%  dominance = Type of dominance considered. Default value is 1.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i)/abs(real(lambda_i)), norm(x_i) = norm(y_i) = 1
%  k_max = When searching subspaces reach this dimension, restart is applied.
%          Default value is Inf.
%  k_min = When restart is applied, only this many vectors are kept.
%          Default value is 2.
%
%  Returns p approximations of dominant poles.
%  Complex conjugate pair is counted as one pole.

  if nargin < 9, dominance = 1; end
  if nargin < 10, k_max = Inf; end
  if nargin < 11, k_min = 2; end

  n = size(A, 1);
  lambdas = zeros(2*p, 1);
  X = zeros(n, 2*p);
  Y = zeros(n, 2*p);
  V = [];
  W = [];
  iters = zeros(p, 1);
  residuals = zeros(1, p * max_iter);

  k = 0;
  p_found = 0;
  step = 0;
  while p_found < p
    % iterations
    iter = 0;
    converged = false;
    while ~converged && iter < max_iter
      iter = iter + 1;
      step = step + 1;
      if size(V, 2) >= k_max
        [V,~] = qr(V * X_til(:,k_min), 0);
        [W,~] = qr(W * Y_til(:,k_min), 0);
      end
      [v, w] = step_solve(E, A, s, b, c);
      V = mgs(V, v);
      W = mgs(W, w);
      [s_til, X_til, Y_til] = sorted_poles(W'*E*V, W'*A*V, W'*b, V'*c, dominance);
      s = s_til(1);
      x = V * X_til(:,1);
      x = x / norm(x);
      y = W * Y_til(:,1);
      y = y / norm(y);
      residuals(step) = max(norm(A*x - s*E*x), norm(y'*A - s*y'*E));
      converged = (residuals(step) < eps);
    end
    iters(p_found+1) = iter;

    [s_next, j_start] = pick_next_start(s, s_til, eps);

    % deflation
    k = k + 1;
    V = V * X_til(:,j_start:end);
    W = W * Y_til(:,j_start:end);
    [x, y] = escale(x, y, E);
    lambdas(k) = s;
    X(:,k) = x;
    Y(:,k) = y;
    [V, W, b, c] = deflate_pole(x, y, V, W, E, b, c);
    if abs(imag(s)) > eps
      k = k + 1;
      lambdas(k) = conj(s);
      X(:,k) = conj(x);
      Y(:,k) = conj(y);
      [V, W, b, c] = deflate_pole(conj(x), conj(y), V, W, E, b, c);
    end

    s = s_next;
    p_found = p_found + 1;
  end

  % reduce the size of preallocated arrays to the number of found states
  lambdas = lambdas(1:k);
  X = X(:,1:k);
  Y = Y(:,1:k);
  residuals = residuals(1:step);
end