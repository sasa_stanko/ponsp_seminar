function [s, x, y] = find_pole(E, A, B, C, dominance)
%FIND_POLE Dominant pole of the system
%
%  (E, A, B, C) = Descriptor system with D = 0.
%  dominance = Type of dominance considered. Default value is 1.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i)/abs(real(lambda_i)), norm(x_i) = norm(y_i) = 1
%
%  s = Poles sorted by their dominance.
%  x = Right eigenvectors for the given poles.
%  y = Left eigenvectors for the given poles.
%
%  Note that x and y are not scaled in any particular way.

  if nargin < 5
    dominance = 1;
  end

  [s_til, X_til, Y_til] = sorted_poles(E, A, B, C, dominance);
  s = s_til(1);
  x = X_til(:,1);
  y = Y_til(:,1);
end