function [E, A, B, C] = get_example(number, j, k, dim)
%EXAMPLE Generate and linearize one of the examples from the article
% "Semi-active damping optimization of  vibrational systems using the
%  parametric dominant pole algorithm".
%
%  number = 1 for example 3.1, 2 for example 3.2
%  j, k = Parameters passed to the function that generates the example.
%  dim = Optional parameter passed to the function that generates the example.

  if number ~= 1 && number ~= 2
    error(strcat('Invalid example number (', num2str(number), ')!'));
  end

  if number == 1
    example = @example31;
  elseif number == 2
    example = @example32;
  end

  if nargin == 3
    [M, alpha_c, K, B2, H1, E2] = example(j, k);
  else
    [M, alpha_c, K, B2, H1, E2] = example(j, k, dim);
  end

  C_crit = make_crit(M, K);
  G = eye(size(B2, 2));
  [E, A, B, C] = linearize(M, alpha_c * C_crit, K, B2, E2, H1, G);

  E = full(E);
  A = full(A);
  B = full(B);
  C = full(C);
end