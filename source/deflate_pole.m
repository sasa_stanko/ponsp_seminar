function [V, W, B, C] = deflate_pole(x, y, V, W, E, B, C)
%DEFLATE_POLE Deflate pole from search spaces and input/output matrices
%
%  x = Right eigenvector of the pole being deflated.
%  y = Left eigenvector of the pole being deflated.
%  V = Right search space.
%  W = Left search space.
%  E, B, C = Matrices from descriptor system.

  % deflate eigentriplet from the system
  B = B - E * x * (y' * B);
  C = C - E' * y * (x' * C);

  % deflate eigentriplet from the search spaces
  ye = y' * E;
  xe = x' * E;
  scale = 1 / (ye * x);
  x = scale * x;
  y = conj(scale) * y;
  V = V - x * (ye * V);
  W = W - y * (xe * W);

  % reorthogonalize search spaces
  [V, ~] = qr(V, 0);
  [W, ~] = qr(W, 0);
end