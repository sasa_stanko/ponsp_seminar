function [R, Rr] = compute_residuals(E, B, C, X, Y, poles)
%COMPUTE_RESIDUALS Compute residuals for given poles and their eigenvectors
%
%  E, B, C = Matrices of descriptor system.
%  X = Right eigenvectors.
%  Y = Left eigenvectors.
%  poles = Poles.
%
%  R = Norms of residuals.
%  Rr = Norms of residuals divided by absolute real part of the eigenvalue.

  R = abs(cnorm(C' * X) .* cnorm(B' * Y) ./ dot(Y, E * X)).';
  Rr = R ./ abs(real(poles));
end