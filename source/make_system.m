function [E, A, b, c] = make_system(n)
%MAKE_SYSTEM Create SISO descriptro system of order n.

  E = eye(n);
  A = make_stable(n);
  b = rand(n, 1);
  c = rand(n, 1);
end