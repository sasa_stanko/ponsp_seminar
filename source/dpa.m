function [lambda, x, y, iter, residuals] = dpa(E, A, b, c, s, eps, max_iter)
%DPA Dominant Pole Algorithm (Rommes - Phd Thesis, Algorithm 2.1)
%
%  (E, A, b, c) = Descriptor SISO system.
%  s = Initial pole estimate.
%  eps = Tolerance << 1.
%  max_iter = Maximum number of iterations.

  iter = 0;
  converged = false;
  residuals = zeros(1, max_iter);
  while ~converged && iter < max_iter
    iter = iter + 1;
    [v, w] = step_solve(E, A, s, b, c);
    s = (w' * A * v) / (w' * E * v);
    x = v / norm(v);
    y = w / norm(w);
    residuals(iter) = max(norm(A*x - s*E*x), norm(y'*A - s*y'*E));
    converged = (residuals(iter) < eps);
  end
  
  lambda = s;
  residuals = residuals(1:iter);
end