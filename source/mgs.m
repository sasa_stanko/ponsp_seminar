function Q = mgs(V, v)
%MGS Modified Gram-Schmidt algorithm to compute orthogonal base for span{V, v}.
%
%  V = Matrix with orthonormal columns.
%  v = Vector with which space span{V} is extended.

  m = size(V, 2);
  tol = 0.1 * norm(v);

  % orthogonalize
  for i = 1:m
    v = v - dot(V(:,i), v) * V(:,i);
  end

  % reorthogonalize if norm dropped to much
  if norm(v) < tol
    v = v - dot(V(:,i), v) * V(:,i);
  end

  v = v / norm(v);
  Q = [V, v];
end