function compare_bode_mimo(E, A, B, C, X, Y, ss, plot_error)
%PLOT_TRANSFER Plot transfer function.
%
%  (E, A, B, C) = Descriptor system.
%  X = Approximation of right dominant eigenvectors.
%  Y = Approximation of left dominant eigenvectors.
%  ss = Grid on which function is plotted.
%  plot_error = Whether to plot relative error.

  if nargin < 7, ss = linspace(0, 10, 200); end
  if nargin < 8, plot_error = true; end

  % projected system
  E_til = Y' * E * X;
  A_til = Y' * A * X;
  B_til = Y' * B;
  C_til = X' * C;

  H = @(s) C' * ((s * E - A) \ B);
  H_til = @(s) C_til' * ((s * E_til - A_til) \ B_til);
  steps = length(ss);
  y = zeros(1, steps);
  y_til = zeros(1, steps);
  r_err = zeros(1, steps);
  for k = 1:steps
    Hs = H(1i * ss(k));
    H_tils = H_til(1i * ss(k));
    [~,y(k),~] = largest_sing(Hs);
    [~,y_til(k),~] = largest_sing(H_tils);
    r_err(k) = norm(Hs - H_tils) / y(k);
  end

  semilogy(ss, abs(y));
  hold on;
  semilogy(ss, abs(y_til));
  if plot_error
    semilogy(ss, r_err);
  end
  hold off;
  if plot_error
    legend('\sigma_1', 'approx', 'rel. error');
  else
    legend('\sigma_1', 'approx');
  end
end