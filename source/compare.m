function [G, results, pole, lambdas] = compare(E, A, B, C, varargin)
%COMPARE Compares different methods for finding dominant pole.
%
%  (E, A, B, C) = Descriptor system with D = 0.
%
%  Parameters
%  dominance = Definition of dominance considered.
%              1 = norm(R_i)/abs(real(lambda_i)), y_i' * E * x_j = delta_{ij}
%              2 = norm(R_i), y_i' * E * x_j = delta_{ij}
%              Default value is 1.
%  conj_pole = Determines if conjugate of the pole is also considered pole.
%             Default value is false.
%  r = Radius of the grid.
%      Default value is 1.5.
%  rx = Half-width of the grid.
%      Default value is r.
%  ry = Half-height of the grid.
%      Default value is r.
%  m = Number of grid points in one direction (i.e. to the left).
%      Default value is 16.
%  eps = Tolerance. Passed to the algorihm.
%      Default value is 1e-8.
%  max_iter = Maximum number of iterations. Passed to the algorithm.
%      Default values is 20.
%  algs = Cell array of algorithms to test.
%      Default value is {@dpa, @rqi, @dpasa}.
%  alg_names = Cell array of algorithm names. Used to display on legends.
%      Default values is {'DPA', 'RQI', 'DPAsa'}.

  % parse parameters
  p = inputParser;
  addParameter(p, 'dominance', 1);
  addParameter(p, 'conj_pole', false);
  addParameter(p, 'r', 1.5);
  addParameter(p, 'rx', NaN);
  addParameter(p, 'ry', NaN);
  addParameter(p, 'm', 16);
  addParameter(p, 'eps', 1e-10);
  addParameter(p, 'max_iter', 20);
  addParameter(p, 'algs', {@dpa, @rqi, @dpasa});
  addParameter(p, 'alg_names', {'DPA', 'RQI', 'DPAsa'});
  parse(p, varargin{:});

  dominance = p.Results.dominance;
  conj_pole = p.Results.conj_pole;
  r = p.Results.r;
  rx = p.Results.rx;
  ry = p.Results.ry;
  m = p.Results.m;
  eps = p.Results.eps;
  max_iter = p.Results.max_iter;
  algs = p.Results.algs;
  alg_names = p.Results.alg_names;

  if isnan(rx), rx = r; end
  if isnan(ry), ry = r; end;

  % get residuals
  [R, Rr, lambdas] = get_residuals(E, A, B, C);

  % determine the dominant pole, assure it is in the upper half-plane
  if dominance == 2
    [~, idx] = max(R);
  else
    [~, idx] = max(Rr);
  end
  pole = lambdas(idx);
  pole = real(pole) + 1i * abs(imag(pole));

  % run tests
  G = make_grid(pole, rx, ry, m);
  k = length(algs);
  results = cell(k, 1);
  for i = 1:k
    [lambdas_, iters] = run_test(E, A, B, C, G, algs{i}, eps, max_iter);
    good = abs(lambdas_ - pole) < eps;
    if conj_pole
      good = good | abs(lambdas_ - conj(pole)) < eps;
    end
    results{i} = struct('lambdas', lambdas_, 'iters', iters, 'good', good);
  end

  % plot eigenvalues and dominant pole
  plot(real(lambdas), imag(lambdas), 'o');
  hold on;
  plot(real(pole), imag(pole), 'ko');

  % plot convergence grid
  % TODO: limit number of algorithms which can be tested
  symbols = '+xds*ph';
  colors = 'brgcmyk';
  description = cell(k+2, 1);
  description{1} = 'Poles';
  description{2} = 'Dominant pole';
  for i = 1:k
    symbol = strcat(colors(i), symbols(i));
    plot(real(G(results{i}.good)), imag(G(results{i}.good)), symbol);
    description{i+2} = strcat(alg_names{i}, ' to target');
  end
  hold off;
  legend(description);

  drawnow;
  pause;

  % plot number of iterations for each starting point
  nG = numel(G);
  x = 1:nG;
  p = get_permutation(results, k);
  description = cell(2*k, 1);
  for i = 1:k
    res = results{i};
    plot(x, res.iters(p), colors(i));
    if i == 1, hold on; end
    plot(x(res.good(p)), res.iters(p(res.good(p))), strcat(colors(i), 'o'));
    description{2*i-1} = alg_names{i};
    description{2*i} = strcat(alg_names{i}, ' to target');
  end
  hold off;
  xlim([1, nG]);
  legend(description);
end

% find permutation for which results look decent enough
function p = get_permutation(results, k)
  n = numel(results{1}.iters);
  total = zeros(n, 1);
  weights = total;
  for i = 1:k
    total = total + results{i}.iters(:);
    M = max(total);
    weights = weights + total / M^(i-1);
  end
  [~, p] = sort(weights);
end