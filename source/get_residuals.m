function [R, Rr, lambdas] = get_residuals(E, A, B, C)
%GET_RESIDUALS Compute residuals and eigenvalues of the descriptor system.
%
%  (E, A, B, C) = Descriptor system with D = 0.
%
%  R = Norms of residuals.
%  Rr = Norms of residuals divided by absolute real part of the eigenvalue.
%  lambdas = Eigenvalues for which the residuals are computed.

  [X, lambdas, Y] = eig(A, E, 'vector');
  [R, Rr] = compute_residuals(E, B, C, X, Y, lambdas);
end