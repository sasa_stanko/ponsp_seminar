function A = make_stable(n)
%MAKE_STABLE Create (almost) stable matrix of order n.
%
%  Returns matrix whose eigenvalues have negative real part with
%  probability one.

  % generate random Q and R
  A = rand(n);
  [Q, R] = qr(A);

  % modify R to have stable eigenvalues
  i = 1;
  while i <= n
	  if i+1 <= n && rand(1) < 0.5
		  amp = norm(R(i:i+1, i:i+1));
		  theta = pi/2 + pi*rand(1);
		  G = [cos(theta), sin(theta); -sin(theta), cos(theta)];
		  R(i:i+1, i:i+1) = amp * G;
		  i = i + 2;
	  else
		  R(i,i) = -abs(R(i,i));
		  i = i + 1;
	  end
  end

  % use R as real Schur form of A
  A = Q * R * Q';
end