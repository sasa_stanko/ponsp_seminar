function compare_bode(E, A, b, c, X, Y, ss, plot_error)
%COMPARE_BODE Compare transfer function with its approximation.
%
%  (E, A, b, c) = Descriptor SISO system.
%  X = Approximation of right dominant eigenvectors.
%  Y = Approximation of left dominant eigenvectors.
%  ss = Grid on which function is plotted.
%  plot_error = Whether to plot relative error.

  if nargin < 7, ss = linspace(0, 10, 200); end
  if nargin < 8, plot_error = true; end

  % projected system
  E_til = Y' * E * X;
  A_til = Y' * A * X;
  b_til = Y' * b;
  c_til = X' * c;

  % evaluate transfer function
  H = @(s) c' * ((s * E - A) \ b);
  H_til = @(s) c_til' * ((s * E_til - A_til) \ b_til);
  y = arrayfun(H, 1i * ss);
  y_til = arrayfun(H_til, 1i * ss);
  r_err = abs((y - y_til) ./ y);

  figure;
  subplot(1,2,1);
  semilogy(ss, abs(y));
  hold on;
  semilogy(ss, abs(y_til));
  if plot_error
    semilogy(ss, abs(r_err));
  end
  hold off;
  if plot_error
    legend('gain', 'approx', 'rel. error');
  else
    legend('gain', 'approx');
  end
  subplot(1,2,2);
  plot(ss, angle(y));
  hold on;
  plot(ss, angle(y_til));
  hold off;
  legend('phase', 'approx');

  pos = get(gcf, 'Position');
  pos(3) = 2.5 * pos(4);
  set(gcf, 'Position', pos);
end