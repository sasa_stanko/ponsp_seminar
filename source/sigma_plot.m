function sigma_plot(E, A, B, C, steps)
%SIGMA_PLOT Sigma plot of the transfer function.
%
%  (E, A, B, C) = Descriptor system with D = 0.
%  steps = Number of points in which transfer function is evaluated.
%          Default is 256.

  if nargin < 5
    steps = 256;
  end

  ss = logspace(-2, 2, steps);
  H = @(s) C' * ((s * E - A) \ B);
  y_M = zeros(1, steps);
  y_m = zeros(1, steps);
  for k = 1:steps
    s = 1i * ss(k);
    [~,S,~] = svd(H(s), 0);
    sigmas = diag(S);
    y_M(k) = sigmas(1);
    y_m(k) = sigmas(end);
  end

  loglog(ss, y_M);
  hold on;
  loglog(ss, y_m);
  hold off;
  legend('Max', 'Min');
end